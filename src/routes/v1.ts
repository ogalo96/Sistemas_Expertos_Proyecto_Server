import * as express from 'express';
import {getAllUsers,
        insertUser,
        inicioUser
} from '../app/ControladorUsuarios';

import {insertDatos, listarArchivos, abrirEditor, abrir, actualizarDatos} from '../app/ControladorDatos';

export default (app) =>{
    const apiRoutes = express.Router();
    const userRoutes = express.Router();
    const datosRoutes = express.Router();

    apiRoutes.use('/user',verificarAutenticacion,userRoutes);
    apiRoutes.use('/datos',verificarAutenticacion,datosRoutes);

    userRoutes.get('/',getAllUsers);

    datosRoutes.post('/guardar',insertDatos);
    datosRoutes.get('/lista',listarArchivos);
    datosRoutes.post('/editar/',abrirEditor);
    datosRoutes.post('/abrir',abrir);
    datosRoutes.post('/actualizar',actualizarDatos);

    app.use('/api',apiRoutes);
    
    app.post("/login",inicioUser);

     
    app.post('/registro',insertUser);
    
    app.get("/logout",function(peticion, respuesta){
        peticion.session.destroy();
        //respuesta.redirect("index.html");
        respuesta.status(200).send({mensaje:"Ha salido con exito"});
    });
    
    app.get('/perfil',verificarAutenticacion,(pet,res)=>{
        res.send({mensaje:"En construccion"});
    });


}
function verificarAutenticacion(peticion, respuesta, next){
	if(peticion.session.correo)
		return next();
	else
		respuesta.status(401).send("ERROR, ACCESO NO AUTORIZADO");
}