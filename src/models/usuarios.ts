import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt-nodejs';

const Schema = mongoose.Schema;

const UsuarioSchema = new Schema({
    local: {
        email:{
            type: String,
            required: true,
            unique: true,
            lowercase: true,
            trim: true
        },
        pass: {
            type: String,
            required: true
        },
        fechaRegistro:{
            type: Date,
            default: Date.now
        }, 
        nombre:{
            type: String,
            required: false
        },
        apellido:{
            type: String,
            required: false
        },
        nick: {
            type: String,
            required: false
        },
    }
   
});
UsuarioSchema.methods.generarHash = (password:string) =>{
    return bcrypt.hashSync(password,bcrypt.genSaltSync(8));
};
/*
UsuarioSchema.methods.validarPass = function (password:string){
    console.log('comparando ',this.local.password);
    if(this === undefined){
        return 'No se puede acceder a local';
    }
    return bcrypt.compareSync(password, this.local.password);
};*/
UsuarioSchema.methods.validarPass = function (password:string,hash:string):boolean{
    if(this === undefined){
        return false;
    }
    return bcrypt.compareSync(password,hash);
};
export default mongoose.model('Usuarios',UsuarioSchema);