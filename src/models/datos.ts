import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const DatosSchema = new Schema({
   datos: {
       type: String,
       required: true
   },
   nombre: {
        type: String,
        required: true
   },
   type:{
        type: String,
        required: true
   },
   mode:{
    type: String,
    //required: true
   },
   fechaCreacion:{
    type: Date,
    default: Date.now
    },
    creador:{
        type: String,
        required: true
    }
});

export default mongoose.model('Datos',DatosSchema);