import * as express from 'express';
import * as mongoose from 'mongoose';
import * as morgan from 'morgan';
import * as session from 'express-session'

import rutas from './routes/v1';
import config from './config/config';

//iniciar express
const app = express();

//iniciar mongoose
mongoose.connect(config.db)
    .then(db => console.log('Conectado a la BD'))
    .catch(err => {
        console.error('mongo error',err)});

//middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(session({
    secret: 'asd.456',
    resave: true,
    saveUninitialized: true
}));

//rutas
app.use(express.static(__dirname+"/public"));//landing
var home = express.static(__dirname+"/www");
/*app.use(function (pet,res,next){
    if(pet.session.codigo){
        home(pet,res,next);
    }
    else{
        next();
    }
});*/
app.use(home);
rutas(app);

function verificarAutenticacion(peticion, respuesta, next){
	if(peticion.session.correo)
		return next();
	else
		respuesta.status(401).send("ERROR, ACCESO NO AUTORIZADO");
}

app.use(function(req, res, next) {
    //res.status(404).render('404.html');
    res.status(404).send('<h1>404 pagina no existe</h1>');
});

app.listen(config.port,()=>{
    console.log(`Server iniciado en el puerto ${config.port}`);
    
});