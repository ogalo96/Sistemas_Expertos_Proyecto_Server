import Datos from '../models/datos';

export function getAllDatos(req, res, next){
    Datos.find()
        .then(data => res.status(200).json(data))
        .catch(error => res.status(500).json({error}));
}

export function insertDatos(req, res, next){
    const datos = new Datos(req.body);
    datos.creador = req.session.codigo;
    if(datos.type == ""){
        datos.type = 'text/plain';
    }
    datos.save()
    .then(res.json({
        status: 'Codigo Guardada',
        datos
    }))
    .catch(error => console.log(error,datos));
}


export function listarArchivos(req, res, next){
    Datos.find({creador: req.session.codigo})
    .then(data => {res.status(200).json({
        data
    })
        console.log(data);
        
    })
    .catch(error => res.status(500).json({error}));
}

export function abrirEditor(req, res, next){
    req.session.editar = req.body.id;
    console.log(req.session.editar);
    res.status(402).json({'edit':req.session.editar});
}

export function abrir(req, res, next){
    if(req.session.editar){
        Datos.findById(req.session.editar)
            .then(data => res.status(200).json(data))
            .catch(error => res.status(500).json({error}));
        req.session.editar = undefined;
    }else{
        res.status(204);
    }
    
}

export function actualizarDatos(req, res, next){
    Datos.findByIdAndUpdate(req.body.id,req.body.data)
    .then(data => res.status(200).json(data))
    .catch(error => console.log(error));
}